package com.atlassian.tutorial.jira.jql;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserIssueHistoryManager;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

/**
 * Echoes the the string passed in as an argument.
 */
@Scanned
public class SortedIssues extends AbstractJqlFunction
{
    private static final Logger log = LoggerFactory.getLogger(SortedIssues.class);
    @ComponentImport
    final private UserIssueHistoryManager issueManager;

    public SortedIssues(UserIssueHistoryManager manager) {
        this.issueManager = manager;
    }

    public MessageSet validate(ApplicationUser searcher, FunctionOperand operand, TerminalClause terminalClause)
    {
        return validateNumberOfArgs(operand, 0);
    }

    public List<QueryLiteral> getValues(QueryCreationContext queryCreationContext, FunctionOperand operand, TerminalClause terminalClause)
    {
        final List<QueryLiteral> literals = new LinkedList<>();
        final List<Issue> issues = this.issueManager.getShortIssueHistory(queryCreationContext.getApplicationUser());

        for (final Issue issue : issues) {
            final String issueKey = issue.getKey();

            try {
                literals.add(new QueryLiteral(operand, issueKey));
            }
            catch (NumberFormatException exception) {
                log.warn(exception.getMessage());
            }
        }

        return literals;

    }

    public int getMinimumNumberOfExpectedArguments()
    {
        return 0;
    }

    public JiraDataType getDataType()
    {
        return JiraDataTypes.ISSUE;
    }
}