package com.atlassian.tutorial.rest;

import com.atlassian.tutorial.rest.sorts.ISortable;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class IssueModel implements ISortable {

    @XmlElement(name = "key")
    private String key;

    @XmlElement(name = "summary")
    private String summary;

    public IssueModel() {
    }

    public IssueModel(String key, String summary) {
        this.key = key;
        this.summary = summary;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String value) {
        this.key = value;
    }

    public String getSummary() {
        return this.summary;
    }

    public void setSummary(String value) {
        this.summary = value;
    }
    public int getNumber() {
        int value = 0;
        try {
            value = Integer.parseInt(this.getKey().split("-")[1]);
        }
        catch (NumberFormatException e) {
            throw e;
        }
        return value;
    }


    @Override
    public int getValue() {
        return this.getNumber();
    }
}