package com.atlassian.tutorial.rest.sorts;

import com.atlassian.tutorial.rest.IssueModel;
import java.util.ArrayList;

public interface ISorter {
    void sort(ISortable[] data, IComparable sort);
}
