package com.atlassian.tutorial.rest.sorts;

public interface IComparable {
    boolean compare(int a, int b);
}
