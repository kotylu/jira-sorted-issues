package com.atlassian.tutorial.rest.sorts;

public interface ISortable {
    int getValue();
}
