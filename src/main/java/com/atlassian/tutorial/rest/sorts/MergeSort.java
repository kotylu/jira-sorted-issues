package com.atlassian.tutorial.rest.sorts;

import com.atlassian.tutorial.rest.IssueModel;
import java.util.ArrayList;

public class MergeSort implements ISorter {

    public void sort(ISortable[] data, IComparable comparator) {
        ISortable[] arr = new ISortable[data.length];
        sort(data, arr, 0, data.length-1, comparator);
    }

    private void sort(ISortable[] data, ISortable[] _data, int start, int end, IComparable comparator) {
        if (start == end)
            return;
        this.sort(data, _data, start, (start+end)/2, comparator);
        this.sort(data, _data, (start+end)/2+1, end, comparator);
        this.merge(data, _data, start, end, comparator);

        for (int i = start; i <= end; i++) {
            data[i] = _data[i];
        }

    }

    private void merge (ISortable[] data, ISortable[] _data, int start, int end, IComparable comparator) {
        int left = start;
        int mid = (start+end)/2;
        int right = mid + 1;
        int help = start;
        while (left <= mid && right <= end) {
            if (comparator.compare(data[left].getValue(), data[right].getValue())) {
                _data[help++] = data[left++];
            }
            else {
                _data[help++] = data[right++];
            }
        }
        while (left <= mid) {
            _data[help++] = data[left++];
        }
        while (right <= end) {
            _data[help++] = data[right++];
        }
    }
}
