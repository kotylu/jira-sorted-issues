package com.atlassian.tutorial.rest;

import com.atlassian.tutorial.rest.sorts.*;
import org.json.*;
import org.slf4j.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.*;
import java.net.*;
import java.util.*;

@Path("/")
public class Issue {

    private static final Logger log = LoggerFactory.getLogger(Issue.class);
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("getSortedIssues")
    public Response getSortedIssues(@HeaderParam("Host") String host, @HeaderParam("Authorization") String auth, @QueryParam("jql") String jql, @QueryParam("sort") String sort)
    {
        IssueModel[] sortedIssues = new IssueModel[1];
        String issues = "";
        if (auth == null) {
            log.warn("non authenticated user");
            return Response.serverError().build();
        }
        if (!(issues = this.getIssuesString(host, jql, auth)).equals(""))
            sortedIssues = this.issuesToArray(this.getIssues(this.getIssuesJSON(issues)));
        if (sortedIssues.length > 0) {
            this.sort(sortedIssues, new MergeSort(), sort);
            return Response.ok(this.issuesFromArray(sortedIssues)).build();
        }
        log.warn("no obj found");
        return Response.serverError().build();
    }

    private IssueModel[] issuesToArray(ArrayList<IssueModel> data) {
        IssueModel[] result = new IssueModel[data.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = data.get(i);
        }
        return result;
    }

    private ArrayList<IssueModel> issuesFromArray(IssueModel[] data) {
        ArrayList<IssueModel> result = new ArrayList<>();
        for (int i = 0; i < data.length; i++) {
            result.add(data[i]);
        }
        return result;
    }
    private void sort(IssueModel[] data, ISorter sortMethod, String sortOrder) {
        switch (sortOrder.toLowerCase()) {
            case "asc":
                sortMethod.sort(data, ((a, b) -> a < b));
                break;
            case "desc":
                sortMethod.sort(data, ((a, b) -> a > b));
                break;
        }
    }

    private String getIssuesString(String host, String jql, String auth) {
        StringBuilder result = new StringBuilder();
        JSONObject json = new JSONObject();
        try {
            URL url = new URL(String.format("http://%s/jira/rest/api/2/search", host));
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Authorization", auth);
            con.setRequestProperty("Content-Type", "application/json");
            json.put("jql", jql);
            json.put("fields", new String[] {"key", "summary"});

            con.setDoOutput(true);
            OutputStream os = con.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
            osw.write(json.toString());

            osw.flush();
            osw.close();
            os.flush();
            os.close();


            BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }

            reader.close();
        } catch (IOException e) {
            log.warn(e.getMessage());
            return "";
        }
        catch (JSONException e) {
            log.warn(e.getMessage());
            return "";
        }

        return result.toString();
    }

    private JSONObject getIssuesJSON(String jsonString) {
        JSONObject result = new JSONObject();
        try {
            result = new JSONObject(jsonString);
        }
        catch (JSONException e) {
            log.warn("Invalid format Str to JSON", e.getCause());
        }

        return result;
    }

    private ArrayList<IssueModel> getIssues(JSONObject json) {
        ArrayList<IssueModel> result = new ArrayList<>();
        try {
            JSONArray issueArr = json.getJSONArray("issues");
            for (int i = 0; i < issueArr.length(); i++) {
                JSONObject issue = issueArr.getJSONObject(i);
                String key = issue.getString("key");
                String summary = issue.getJSONObject("fields").getString("summary");
                result.add(new IssueModel(key, summary));
            }
        }
        catch (JSONException e) {
            log.warn("Invalid format JSON to Issue", e.getCause());
        }

        return result;
    }
}