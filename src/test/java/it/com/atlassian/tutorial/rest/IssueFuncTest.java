package it.com.atlassian.tutorial.rest;

import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.mockito.Mockito;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import com.atlassian.tutorial.rest.Issue;
import com.atlassian.tutorial.rest.IssueModel;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;

public class IssueFuncTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void messageIsValid() {

        String baseUrl = System.getProperty("baseurl");
        String resourceUrl = baseUrl + "/rest/issue/1.0/message";

        RestClient client = new RestClient();
        Resource resource = client.resource(resourceUrl);

        IssueModel message = resource.get(IssueModel.class);

        assertEquals("wrong message","Hello World",message.getKey());
    }
}
